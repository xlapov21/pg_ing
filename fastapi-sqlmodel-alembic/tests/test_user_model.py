import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlmodel import Session, SQLModel
from project.app.models.user import User, UserCreate
from project.db import DATABASE_URL, init_db, get_sync_session
from project.app.models.user import User
from project.app.models.user import create_user, get_users, update_user, delete_user, get_user

TEST_DATABASE_URL = "sqlite:///:memory:"
engine = create_engine(TEST_DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

@pytest.fixture
def db():
    SQLModel.metadata.create_all(bind=engine)
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.rollback()
        db.close()

def test_create_user(db: Session):
    user_data = {"name": "John Doe", "email": "john.doe@example.com", "password": "securepassword"}
    user_create = UserCreate(**user_data)
    
    # Convert Pydantic model to SQLAlchemy ORM model
    user_orm = User(**user_create.model_dump())  # Updated to use model_dump
    
    db.add(user_orm)  # Add the user instance to the session
    db.commit()   # Commit the changes to the database

    db.refresh(user_orm)  # Refresh the user instance to get the values from the database

    assert user_orm.id is not None  # Check that the user has been assigned an ID
    assert user_orm.name == "John Doe"
    assert user_orm.email == "john.doe@example.com"

def test_get_users(db: Session):
    # Создаем пользователя
    user_data = {"name": "John Doe", "email": "john.doe@example.com", "password": "securepassword"}
    user_create = UserCreate(**user_data)
    user_orm = create_user(db, user_create)

    # Получаем всех пользователей
    users = get_users(db)

    # Проверяем, что список пользователей не пустой
    assert users
    # Проверяем, что первый пользователь в списке имеет ожидаемые атрибуты
    assert users[0].id is not None
    assert users[0].name == user_data["name"]
    assert users[0].email == user_data["email"]

# def test_update_user(db: Session):
#     # Создаем пользователя
#     user_data = {"name": "Alice", "email": "alice@example.com", "password": "alicepassword"}
#     user_create = UserCreate(**user_data)
#     created_user = create_user(db, user_create)

#     # Обновляем данные пользователя
#     updated_data = {"name": "Alice Smith", "email": "alice.smith@example.com", "password": "newpassword"}
#     update_user(db, created_user.id, updated_data)

#     # Получаем обновленного пользователя из базы данных
#     updated_user = User.get(db, created_user.id)

#     # Проверяем, что пользователь был успешно обновлен
#     assert updated_user is not None
#     assert updated_user.id == created_user.id
#     assert updated_user.name == updated_data["name"]
#     assert updated_user.email == updated_data["email"]
#     assert updated_user.password == updated_data["password"]

def test_delete_user(db: Session):
    # Создаем пользователя
    user_data = {"name": "Bob", "email": "bob@example.com", "password": "bobpassword"}
    user_create = UserCreate(**user_data)
    user_orm = create_user(db, user_create)

    # Удаляем пользователя
    delete_user(db, user_orm.id)

    # Получаем пользователя из базы данных
    deleted_user = User.get_user(db, user_orm.id)  # Обновляем вызов функции get_user

    # Проверяем, что пользователь успешно удален
    assert deleted_user is None