import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session as _Session, sessionmaker
from sqlmodel import Session, SQLModel
from project.app.models.song import Song, SongCreate
from project.db import DATABASE_URL, init_db, get_sync_session

TEST_DATABASE_URL = "sqlite:///:memory:"
engine = create_engine(TEST_DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
# /home/sirius/Рабочий стол/pg_ing/fastapi-sqlmodel-alembic/project
@pytest.fixture
def db():
    SQLModel.metadata.create_all(bind=engine)
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.rollback()
        db.close()

def test_create_song(db):
    song_data = {"name": "Test Song", "artist": "Test Artist", "year": 2022}
    song = SongCreate(**song_data)

    created_song = Song.create(db, song)

    assert created_song.id is not None
    assert created_song.name == song_data["name"]
    assert created_song.artist == song_data["artist"]
    assert created_song.year == song_data["year"]

def test_get_song(db):
    song_data = {"name": "Test Song", "artist": "Test Artist", "year": 2022}
    song = SongCreate(**song_data)
    created_song = Song.create(db, song)

    retrieved_song = Song.get(db, created_song.id)

    assert retrieved_song.id == created_song.id
    assert retrieved_song.name == created_song.name
    assert retrieved_song.artist == created_song.artist
    assert retrieved_song.year == created_song.year


def test_delete_song(db):
    song_data = {"name": "Test Song", "artist": "Test Artist", "year": 2022}
    song = SongCreate(**song_data)
    created_song = Song.create(db, song)
    assert Song.get(db, created_song.id) is not None
    Song.delete(db, created_song.id)
    assert Song.get(db, created_song.id) is None