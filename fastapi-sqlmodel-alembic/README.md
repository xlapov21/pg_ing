# FastAPI + SqlAlchemy + Alembic

Проект "pg_ing" разработан студентами для души. 
В данном репозитории представлена сервная часть веб-приложения.

## Какие технологии использованы?

SQLAlchemy, SQLModel, Postgres, Alembic, Docker, Prometheus, Flake8, MyPy, PyTest и ещё миллион всего

## А что в проекте есть сейчас?

- Авторизация пользователей
- Аутентификация пользователей
- Хм... (пока вроде всё)
- Тесты

## Как же поднять проект?

Создайте в папке fastapi-sqlmodel-alembic файл .env, пример:
```
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_DB=foo
POSTGRES_PORT=5432
POSTGRES_HOST=postgresql+asyncpg

WORKER_COUNT=1
WEB_PORT=8000
WEB_HOST=0.0.0.0

PROMETHEUS_PORT=9090
REDIS_PORT=6378
```

Далее запустите команду:
```sh
$ docker-compose up --build
```

А можно и так:
```sh
$ docker-compose up -d --build
```
Тогда убить демона надо будет так:
```sh
docker-compose down -v
```


## Сервер будет тут: http://localhost:8000

## Swagger если что тут: http://localhost:8000/swagger

## Чтобы посмотреть метрики: http://localhost:8000/metrics

## Чтобы зайти в прометеус: http://localhost:9090/


## Для переключения уровня дебага:

Воспользуйтесь эндпоинтом, где log_level - нужный уровень дебага
```
/set_log_level/{log_level}
```


## Некоторые команды для разработки проекта 

Создать первый файл миграции:

```sh
docker-compose exec web alembic revision --autogenerate -m "init"
```

Создать новый файл миграции:

```sh
docker-compose exec web alembic revision --autogenerate -m "add {name}"
```

После каждого нового файла применить миграцию:
```sh
docker-compose exec web alembic upgrade head
```

Чтобы запустить flake8:
```sh
poetry run flake8 .
```

Чтобы запустить mypy:
```sh
poetry run mypy .
```


