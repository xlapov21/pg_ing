import os
import subprocess
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

DATABASE_URL = "postgresql+asyncpg://postgres:postgres@localhost:5432/foo"

# Use create_async_engine to create an asynchronous engine
engine = create_async_engine(DATABASE_URL, echo=True, future=True)

Session = sessionmaker(bind=engine)

def get_sync_session():
    return Session()
def init_db():
    os.chdir("/home/sirius/Рабочий стол/pg_ing/fastapi-sqlmodel-alembic/project")

    subprocess.run('alembic upgrade head', shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    subprocess.run('alembic upgrade head',
                   shell=True,
                   check=True,
                   stdout=subprocess.PIPE,
                   stderr=subprocess.PIPE,
                   text=True)

def seeds():
    pass

async def get_session() -> AsyncSession:
    async_session = sessionmaker(
        engine, class_=AsyncSession, expire_on_commit=False
    )
    async with async_session() as session:
        yield session