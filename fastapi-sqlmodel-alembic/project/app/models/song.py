from sqlalchemy.orm import Session
from sqlmodel import SQLModel, Field
from typing import Optional

class SongBase(SQLModel):
    id: int = None
    name: str
    artist: str
    year: Optional[int] = None


class Song(SongBase, table = True, extend_existing = True):
    id: int = Field(default = None, nullable = False, primary_key = True)

    @classmethod
    def create(cls, db: Session, song: SongBase):
        db_song = cls(**song.model_dump())
        db.add(db_song)
        db.commit()
        db.refresh(db_song)
        return db_song

    @classmethod
    def get(cls, db: Session, song_id: int):
        return db.query(cls).filter(cls.id == song_id).first()

    @classmethod
    def delete(cls, db: Session, song_id: int):
        db.query(cls).filter(cls.id == song_id).delete()
        db.commit()


class SongCreate(SongBase):
    pass
