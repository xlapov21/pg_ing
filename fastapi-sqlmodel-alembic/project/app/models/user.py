
from sqlmodel import SQLModel, Session,  Field, select
from pydantic import BaseModel
from sqlmodel.ext.asyncio.session import AsyncSession

class UserLogin(BaseModel):
    email: str
    password: str

class UserBase(SQLModel):
    name: str
    email: str
    password: str

class UserCreate(UserBase):
    pass


class User(UserBase, table=True):
    id: int = Field(default=None, nullable=False, primary_key=True)

    @classmethod
    def create_user(db: Session, user: UserCreate):
        db_user = User(**user.model_dump())
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user

    @classmethod
    def get_users(db: Session):
        return db.query(User).all()

    @classmethod
    def update_user(cls, db: Session, user_id: int, user_data: dict):
        db.query(cls).filter(cls.id == user_id).update(user_data)
        db.commit()

    @classmethod
    def delete_user(db: Session, user_id: int):
        db.query(User).filter(User.id == user_id).delete()
        db.commit()

    @classmethod
    def get_user(cls, db: Session, user_id: int):
        return db.query(cls).filter(cls.id == user_id).first()

    @classmethod
    def get(cls, db: Session, user_id: int):
        return db.query(cls).filter(cls.id == user_id).first()


def create_user(db: Session, user: UserCreate):
    db_user = User(**user.model_dump())
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_users(db: Session):
    return db.query(User).all()



def update_user(db: Session, user_id: int, user_data: dict):
    db.query(User).filter(User.id == user_id).update(user_data)
    db.commit()


def delete_user(db: Session, user_id: int):
    db.query(User).filter(User.id == user_id).delete()
    db.commit()


def get_user(cls, db: Session, user_id: int):
    return db.query(cls).filter(cls.id == user_id).first()



def get(cls, db: Session, user_id: int):
    return db.query(cls).filter(cls.id == user_id).first()

async def get_user_by_email(email: str, session: AsyncSession):
    query = select(User).where(User.email == email)
    result = await session.execute(query)
    return result.scalar_one_or_none()