from fastapi import Depends, FastAPI, HTTPException, status, Header
from sqlmodel import select
from sqlmodel.ext.asyncio.session import AsyncSession
from prometheus_fastapi_instrumentator import Instrumentator
from app.db import get_session, init_db
from app.models.song import Song, SongCreate
from app.models.user import User, UserCreate, UserLogin
from app.metrics import PrometheusMiddleware, metrics
from fastapi.middleware.cors import CORSMiddleware
import logging
from typing import List
import app.services.jwt as jwt
from sqlalchemy.exc import IntegrityError
import datetime


app = FastAPI(docs_url='/swagger')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

app.add_route('/metrics', metrics)
app.add_middleware(PrometheusMiddleware, filter_unhandled_paths=True)
Instrumentator().instrument(app).expose(app)
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


@app.on_event('startup')
def on_startup():
    init_db()


async def authenticate_user(token: str = Header(...), session: AsyncSession = Depends(get_session)):
    try:
        token_data = jwt.decode(token)
        if datetime.datetime.utcfromtimestamp(token_data['exp']) < datetime.datetime.utcnow():
            raise HTTPException(status_code=401, detail="Unauthorized")

        user = await session.get(User, token_data['id'])
        if user:
            return user
        else:
            raise HTTPException(status_code=401, detail="Unauthorized")
    except Exception as e:
        raise HTTPException(status_code=500, detail="Authentication failed: {e}")

@app.get('/ping')
async def pong():
    logger.debug('Это ping-pong! DEBUG')
    logger.info('Это ping-pong! INFO')
    return {'ping": "pong!'}


@app.get('/set_log_level/{log_level}')
def set_log_level(log_level: str):
    logging.getLogger().setLevel(log_level.upper())
    return {'message': f'Уровень логирования изменен на {log_level.upper()}'}


@app.get('/songs', response_model=List[Song])
async def get_songs(session: AsyncSession = Depends(get_session)):
    try:
        logger.debug("Fetching songs from the database...")
        result = await session.execute(select(Song))
        songs = result.scalars().all()
        logger.info(f"Fetched {len(songs)} songs from the database.")
        response_songs = [
            Song(name=song.name, artist=song.artist, year=song.year, id=song.id)
            for song in songs
        ]
        return response_songs

    except Exception as e:
        logger.error(f"An error occurred while fetching songs: {str(e)}")
        raise


@app.post('/registration')
async def create_user(user: UserCreate, session: AsyncSession = Depends(get_session)):
    try:
        logger.debug("Creating a new user...")

        user_db = User(name=user.name, email=user.email, password=user.password)
        session.add(user_db)
        await session.commit()
        await session.refresh(user_db)

        logger.info(f"User '{user_db.name}' with email '{user_db.email}' successfully registered.")

        return str(jwt.encode({'id': user_db.id}))

    except IntegrityError as e:
        session.rollback()
        logger.error(f"IntegrityError occurred while creating a user: {str(e)}")
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Email '{user.email}' already registered"
        )

    except Exception as e:
        session.rollback()
        logger.error(f"An error occurred while creating a user: {str(e)}")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Internal server error: {str(e)}"
        )

@app.post('/login')
async def get_user(login_data: UserLogin, session: AsyncSession = Depends(get_session)):
    try:
        logger.debug(f"Attempting to log in user with email: {login_data.email}")

        user = await get_user_by_email(login_data.email, session)
        if user:
            if user.password == login_data.password:
                logger.info(f"User with email '{login_data.email}' successfully logged in.")
                return str(jwt.encode({'id': user.id}))
            else:
                logger.debug(f"Incorrect password for user with email: {login_data.email}")
                raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect password")
        else:
            logger.debug(f"User with email '{login_data.email}' not found.")
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")

    except Exception as e:
        logger.error(f"An error occurred while processing login request: {str(e)}")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Internal server error: {str(e)}"
        )


@app.post('/songs')
async def add_song(song: SongCreate, user: User = Depends(authenticate_user), session: AsyncSession = Depends(get_session)):
    try:
        logger.debug(f"Adding a new song '{song.name}' by '{song.artist}' for user '{user.name}'.")

        new_song = Song(name=song.name, artist=song.artist, year=song.year)
        session.add(new_song)
        await session.commit()
        await session.refresh(new_song)
        logger.info(f"Song '{new_song.name}' by '{new_song.artist}' added successfully for user '{user.name}'.")
        return {"song": new_song, "new_token": str(jwt.encode({'id': user.id}))}

    except Exception as e:
        logger.error(f"An error occurred while adding a song: {str(e)}")
        raise HTTPException(status_code=500, detail=str(e))
