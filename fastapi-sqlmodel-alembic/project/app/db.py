import os

from sqlmodel import create_engine
from sqlalchemy.ext.asyncio import AsyncSession, AsyncEngine
import subprocess
from json import load
from sqlalchemy.orm import sessionmaker
from app.models.user import User, UserCreate, create_user
from app.models.song import Song, SongCreate

DATABASE_URL = os.environ.get("DATABASE_URL")

engine = AsyncEngine(create_engine(DATABASE_URL, echo = True, future = True))
Session = sessionmaker(bind = engine)


def get_sync_session():
    return Session()

    
def init_db():
    subprocess.run('alembic upgrade head',
                   shell=True,
                   check=True,
                   stdout=subprocess.PIPE,
                   stderr=subprocess.PIPE,
                   text=True)
#     seeds()
#
# def seeds():
#     file_path = './seeds/users.json'
#     session = get_sync_session()
#     with open(file_path, 'r') as file:
#         for data in load(file):
#             create_user(session, UserCreate(**data))
#     file_path = './seeds/songs.json'
#     with open(file_path, 'r') as file:
#         for data in load(file):
#             Song.create(session, SongCreate(**data))
#     session.close()

async def get_session() -> AsyncSession:
    async_session = sessionmaker(
        engine, class_ = AsyncSession, expire_on_commit = False
    )
    async with async_session() as session:
        yield session