import jwt
import datetime

SECRET_KEY = 'qwerty123'

def encode(data):
    data['exp'] = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)  # Время истечения токена (30 минут)
    return jwt.encode(data, SECRET_KEY, algorithm="HS256")


def decode(token):
    try:
        return jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
    except jwt.ExpiredSignatureError:
        print("Token has expired.")
    except jwt.InvalidTokenError:
        print("Invalid token.")